package kafka_pubsub

import (
        "fmt"
        "github.com/Shopify/sarama"
	"gopkg.in/resty.v1"
        "encoding/json"
	"encoding/binary"
	// "gopkg.in/resty.v1"
	"github.com/linkedin/goavro"
        "strings"
	"strconv"
	"github.com/fatih/structs"
	"reflect"
	"io/ioutil"
)

var producer sarama.AsyncProducer

func InitKafkaProducer() (err error) {

    // INITIATE KAFKA PRODUCER
    config := sarama.NewConfig()
    //config.Producer.Retry.Max = 5
    config.Producer.RequiredAcks = sarama.WaitForAll
    kafkaURL := strings.Split(brokers, ",")
    producer, err = sarama.NewAsyncProducer(kafkaURL, config)
    if err != nil {
        panic(err)
    }
    return
}

func getRawSchema(avroSchema string) string {

    rawSchema := avroSchema
    rawSchema = strconv.Quote(rawSchema)
    rawSchema = strings.Replace(rawSchema, "\\n", "", -1)
    rawSchema = strings.Replace(rawSchema, "\\t", "", -1)
    //rawSchema = strings.Replace(rawSchema, "\"", "\"", -1)

    return rawSchema
}

func InitKafkaProducerAvro(topic string, avroFile string) (err error) {
    // Initiate avro schema
    var avroSchema string
    _, ok := listSchema[topic]
    if !ok {
        avroByte, err := ioutil.ReadFile(avroFile)
        if err != nil {
            fmt.Print(err)
        }
        avroSchema = string(avroByte)
        listSchema[topic] = avroSchema
    } else {
        avroSchema = listSchema[topic]
    }

    // Initiate Kafka Producer
    config := sarama.NewConfig()
    //config.Producer.Retry.Max = 5
    config.Producer.RequiredAcks = sarama.WaitForAll
    kafkaURL := strings.Split(brokers, ",")
    producer, err = sarama.NewAsyncProducer(kafkaURL, config)
    if err != nil {
        panic(err)
    }

    rawSchema := getRawSchema(avroSchema)
    //fmt.Println(rawSchema)

    // Register new schema
    schemaURL := fmt.Sprintf("{\"schema\":%s}",rawSchema)
    regURL := fmt.Sprintf("%s/subjects/%s-value/versions",schemaregistryURL,topic)
    resty.R().
		SetHeader("Content-Type", "application/vnd.schemaregistry.v1+json").
                SetBody(schemaURL).
                Post(regURL)

    // Initiate avro schema id
    regURL = fmt.Sprintf("%s/subjects/%s-value/versions/latest",schemaregistryURL,topic)
    resp,err := resty.R().
                Get(regURL)
    if err!=nil {
        panic(err)
    }
    var body map[string]interface{}
    json.Unmarshal(resp.Body(), &body)
    id, ok := body["id"].(float64)
    listSchemaId[topic] = uint32(id)
    //fmt.Printf("%v\n", body["id"])
    //fmt.Printf("%v\n", id)

    return
}

func ProduceKafka(topic string, data interface{}) (err error) {

    value, err := json.Marshal(data)
    if err!=nil {
        panic(err)
    }
    //strTime := strconv.Itoa(int(time.Now().Unix()))
    msg := &sarama.ProducerMessage{
        Topic: topic,
        //Key:   sarama.StringEncoder(strTime),
	Value: sarama.ByteEncoder([]byte(value)),
    }

    select {
	case producer.Input() <- msg:
		fmt.Println("Produce message")
	case err := <-producer.Errors():
		fmt.Println("Failed to produce message:", err)
    }

    return
}

func ProduceKafkaAvro(topic string, dataInput interface{}) (err error) {

	avroSchema := listSchema[topic]

	codec, err := goavro.NewCodec(avroSchema)
	if err != nil {
            fmt.Println(err)
	}

	//fmt.Printf("%v\n", dataInput)
	var data interface{}
	if !(strings.Contains(reflect.TypeOf(dataInput).String(), "map")) {
		s := structs.New(dataInput)
		data = s.Map()
	} else {
		data = dataInput
	}
	//fmt.Printf("%v\n", data)

	valByte, err := codec.BinaryFromNative(nil, data)
	if err != nil {
            fmt.Println(err)
	}

	var msgByte []byte
	magicByte := []byte{0}
	//schemaByte := []byte{0x0,0x0,0x0,0x53}
	id := listSchemaId[topic]
	schemaByte := make([]byte, 4)
	binary.BigEndian.PutUint32(schemaByte, id)
	msgByte = append(magicByte, schemaByte...)
	msgByte = append(msgByte, valByte...)

	//fmt.Printf("Msg Producer (bytes): %#v\n", schemaByte)
	//fmt.Printf("Msg Producer (bytes): %#v\n", msgByte)
	//fmt.Printf("Msg Producer (record): %v\n", data)

	//strTime := strconv.Itoa(int(time.Now().Unix()))
        msg := &sarama.ProducerMessage{
            Topic: topic,
            //Key:   sarama.StringEncoder(strTime),
            Value: sarama.ByteEncoder(msgByte),
        }
        select {
                case producer.Input() <- msg:
                        fmt.Println("Produce message")
                case err := <-producer.Errors():
                        fmt.Println("Failed to produce message:", err)
        }
        return

}

