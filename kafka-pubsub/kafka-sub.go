package kafka_pubsub

import (
        "fmt"
        //"github.com/confluentinc/confluent-kafka-go/kafka"
        "encoding/json"
	"github.com/Shopify/sarama"
	"gopkg.in/resty.v1"
	//"github.com/elodina/go-avro"
	"github.com/linkedin/goavro"
	"strings"
	"io/ioutil"
)

// *** Modify your function here ******************** //
func useConsumer(msg *sarama.ConsumerMessage, topic string, urlList []string) {
        var data interface{}
        json.Unmarshal(msg.Value, &data)

	fmt.Printf("**********\nMessage:\n%+v\n", data)
	for _,url := range urlList {
	    fmt.Printf("Send to: %s\n", url)
	    go func() {
            resty.R().
                SetBody(data).
                Post(url)
	    }()
	}
	fmt.Printf("**********\n\n")
}

func useConsumerAvro(msg *sarama.ConsumerMessage, topic string, avroSchema string, urlList []string) (err error) {

	msgByte := msg.Value[5:]

	codec, err := goavro.NewCodec(avroSchema)
	if err != nil {
            fmt.Println(err)
	}

	var data interface{}
	data, _, err = codec.NativeFromBinary(msgByte)
	if err != nil {
            fmt.Println(err)
	}

	//fmt.Printf("Msg Consumer (bytes): %#v\n", msgByte)
	//fmt.Printf("Msg Consumer (record): %v\n", data)

	fmt.Printf("**********\nMessage:\n%+v\n", data)
	for _,url := range urlList {
	    fmt.Printf("Send to: %s\n", url)
	    go func() {
            resty.R().
                SetBody(data).
                Post(url)
	    }()
	}
	fmt.Printf("**********\n\n")
	return
}

func ConsumeKafka(topic string, url []string) (err error) {

        config := sarama.NewConfig()
	config.Consumer.Return.Errors = true

	kafkaURL := strings.Split(brokers, ",")

	consumer, err := sarama.NewConsumer(kafkaURL, config)
	if err != nil {
		panic(err)
	}

	partitionConsumer, err := consumer.ConsumePartition(topic, 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

        for m := range partitionConsumer.Messages() {
		useConsumer(m, topic, url)
	}

    return
}

func ConsumeKafkaAvro(topic string, avroFile string, url []string) (err error) {

	// Initiate avro schema
	var avroSchema string
	_, ok := listSchema[topic]
	if !ok {
	    avroByte, err := ioutil.ReadFile(avroFile)
            if err != nil {
	        fmt.Print(err)
	    }
	    avroSchema = string(avroByte)
	    listSchema[topic] = avroSchema
	    //fmt.Println("Create new schema")
	} else {
	    avroSchema = listSchema[topic]
	    //fmt.Println("Use existing schema")
	}

        config := sarama.NewConfig()
	config.Consumer.Return.Errors = true

	kafkaURL := strings.Split(brokers, ",")

	consumer, err := sarama.NewConsumer(kafkaURL, config)
	if err != nil {
		panic(err)
	}

	partitionConsumer, err := consumer.ConsumePartition(topic, 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	/*
        for m := range partitionConsumer.Messages() {
		m_Magic := m.Value[0]
		m_SchemaID := m.Value[1:4]
		m_Value := m.Value[5:]
		fmt.Printf("Magic Byte: %#v\n", m_Magic)
		fmt.Printf("SchemaID: %#v\n", m_SchemaID)
		fmt.Printf("Record: %#v\n", m_Value)
                useConsumerAvro(m, topic, avroSchema, url)
	}
	*/
	for {
	    select {
	        case err = <-partitionConsumer.Errors():
	            fmt.Println(err)
		case m := <-partitionConsumer.Messages():
		    useConsumerAvro(m, topic, avroSchema, url)
	    }
	}

    return
}
