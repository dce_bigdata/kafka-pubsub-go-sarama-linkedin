package main

import (
	"sync"
	. "../../kafka-pubsub"
)

func ConsumeKafkaAvroWrapper(topic string, avroFile string, url []string) {
        var wg sync.WaitGroup
        wg.Add(1)
        go func() {
                defer wg.Done()
                ConsumeKafkaAvro(topic, avroFile, url)
        }()
        wg.Wait()
}

func main() {


  //Producer

  // Data Input has struct type
  topic := "test5"
  avroFile := "../avro-schema/test5.avsc"
  type Message struct {
    Name string `json="Name"`
    Age int `json="Age"`
  }
  data := &Message{Name:"Data dalam", Age:57}

  InitKafkaProducerAvro(topic, avroFile)
  ProduceKafkaAvro(topic,data)

  // Consumer
  url := []string{}
  ConsumeKafkaAvroWrapper(topic, avroFile, url)

}
