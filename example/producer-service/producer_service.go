package main

import (
	"github.com/gin-gonic/gin"
	//"fmt"
	"net/http"
	. "../../kafka-pubsub"
)

var router *gin.Engine

func RunProduce(c *gin.Context) {
    var data    interface{}
    topic := c.Param("topic")
    if err := c.ShouldBindJSON(&data); err == nil {
	//ProduceKafka(topic, data) // Produce data to Kafka topic
	ProduceKafkaAvro(topic, data) // Produce data to Kafka topic
        c.JSON(http.StatusOK, gin.H{"status": http.StatusOK})
    } else {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
    }
}

func main() {

  // Set the router as the default one provided by Gin
  router = gin.Default()

  // Initialize kafka
  //InitKafkaProducer()
  topic := "test4"
  avroFile := "../avro-schema/test4.avsc"
  InitKafkaProducerAvro(topic, avroFile)

  // Initialize the routes
  router.POST("/:topic", RunProduce)

  // Start serving the application
  router.Run("0.0.0.0:8020")
}
